# 문제점 수정 사항

https 접속 시 다음과 같은 에러가 발생하게 되는데 해당 건에 대한 처리는 menuconfig에 추가적인 옵션을 추가해야 한다.


```
Check esp_tls API reference
E (2020) esp-tls-mbedtls: Failed to set client configurations, returned [0x8017] (ESP_ERR_MBEDTLS_SSL_SETUP_FAILED)
E (2030) esp-tls: create_ssl_handle failed
E (2030) esp-tls: Failed to open new connection
E (2040) TRANSPORT_BASE: Failed to open a new connection
E (2050) HTTP_CLIENT: Connection failed, sock < 0
E (2050) image_download: HTTP GET request failed: ESP_ERR_HTTP_CONNECT
```


### Configure the project
CONFIG_ESP_TLS_INSECURE=y
CONFIG_ESP_TLS_SKIP_SERVER_CERT_VERIFY=y
